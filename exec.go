package main

import "log"
import "net/http"
import "os"

import "gitlab.com/aslatter/fetchtest/internal"

func main() {
	logger := log.New(os.Stderr, "", log.LstdFlags)
	internal.Test(&http.Client{}, logger)
}

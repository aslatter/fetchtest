package app

import "log"
import "net/http"

import "gitlab.com/aslatter/fetchtest/internal"

import "google.golang.org/appengine"
import "google.golang.org/appengine/urlfetch"

func init() {
	http.HandleFunc("/", handler)
}

func handler(w http.ResponseWriter, r *http.Request) {
	logger := log.New(w, "", log.LstdFlags)
	ctx := appengine.NewContext(r)
	client := urlfetch.Client(ctx)
	internal.Test(client, logger)
}

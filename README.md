	$ goapp version
	go version go1.6 (appengine-1.9.35) linux/amd64

I'm having trouble sending some cookies out using `urlfetch.Client`. My test project at gitlab.com/aslatter/fetchtest has an example of going the same requests with both `urlfetch.Client` and `http.DefaultClient`, and I'm not getting the same cookies back. I haven't tried any of this running in a live instance of App Engine.

Default client from command line (`go install gitlab.com/aslatter/fetchtest && fetchtest`):

	2016/04/17 20:19:24 Site: http://www.google.com/
	2016/04/17 20:19:24 Response Cookies: NID=78=L_SDcFQ0Z4Cc7J9E1V3i0EVaJ4hJYHwsoF7LFaaqX_36wtL1rAZ--yBZtIfU8lC6hgkCbz5GGSwI_BpyXD38Q46yVi4KB7ycUfHahfm3EUZbHiN3DzzpHL2TeBSCYUzsZvAChJqbmXCOA3k; expires=Tue, 18-Oct-	2016 01:19:24 GMT; path=/; domain=.google.com; HttpOnly
	2016/04/17 20:19:26 Site: https://madison.legistar.com/Calendar.aspx
	2016/04/17 20:19:26 Response Cookies: Setting-205-Calendar Options=info; expires=Fri, 17-Apr-2116 07:00:00 GMT; path=/

I'm getting cookies back from `google.com` and `madison.legistar.com`.

However when using `urlfetch.Client` with the dev server (`goapp serve app`):

	2016/04/18 01:20:55 Site: http://www.google.com/
	2016/04/18 01:20:55 Response Cookies: NID=78=Ks--47OdF0Pk6FtSs1Ls0qFW8EVQAeFCQKwrAM5wbKnTF3Yp85obLHfZ5at1dCbxO-XyTagSGIg5S2HryH6hxdFJd0q2RSAiDAo_8dvfEYvMsliz9ynzCjlCiimj9aBTo1ut1P6h9jRwlg; expires=Tue, 18-Oct-2016 01:20:55 GMT; path=/; domain=.google.com; HttpOnly
	2016/04/18 01:20:56 Site: https://madison.legistar.com/Calendar.aspx
	2016/04/18 01:20:56 Response Cookies:

I still get my cookies from `google.com`, but my cookies from `madison.legistar.com` get eaten.

I'm also having trouble setting sending similar cookies via `request.Header.Set("Cookie", "Setting-205-Calendar Year=Next Month")` using urlfetch.Client - they seeem to send fine with http.DefaultClient, however.
That's harder to set up a simple test for, though.

If the urlfetch transport is trying to parse "Set-Cookie" and "Cookie" headers with `"net/http".Cookie` it's probably running into the fact that the `http.Cookie` parsing can't handle cookie-names with spaces in them.
package internal

import "log"
import "net/http"

func Test(client *http.Client, logger *log.Logger) {

	// make sure we don't have a cookie jar, just in case
	client.Jar = nil

	sites := []string{
		"http://www.google.com/",
		"https://madison.legistar.com/Calendar.aspx",
	}

	for _, urlString := range sites {
		resp, _ := client.Head(urlString)
		logger.Printf("Site: %s", urlString)
		logger.Printf("Response Cookies: %+v", resp.Header.Get("Set-Cookie"))
	}

}
